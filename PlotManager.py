# -*- coding: utf-8 -*-
"""
Created on Sat Nov 14 16:45:03 2020

@author: Simranjot Kaur Khera
"""
import matplotlib
import pandas as panda
from databaseManager import DatabaseManager
from decimal import *
import seaborn as sns
import matplotlib.pyplot as plt 
class PlotManager:
    
    def __init__(self, db, countryName):
        self.dbManager= db
        self.countryName = countryName
        #self.plotDifferenceBetweenDeathTolls()
    
    #This function returns dataframe with the deathtoll column of the country given
    #It substracts 2 dataframes to return the difference in deathtolls
    def getDataFrameWithTollDifference(self, todayDate, yesterday2Date, currentCountryName):
        
        countryQuery = f"SELECT deaths1Mpop FROM countryData WHERE country ='{currentCountryName}' AND dataDate='{todayDate}'"
        frameData = panda.read_sql_query(countryQuery, self.dbManager.connectionObject)
        todayDataFrame = panda.DataFrame(frameData)
        
        countryQuery = f"SELECT deaths1Mpop FROM countryData WHERE country ='{currentCountryName}' AND dataDate='{yesterday2Date}'"
        frameData = panda.read_sql_query(countryQuery, self.dbManager.connectionObject)
        yesterday2DataFrame = panda.DataFrame(frameData)
        
        combinedDataFrame = todayDataFrame - yesterday2DataFrame
        newIndex={0:currentCountryName}
        combinedDataFrame.rename(index=newIndex, inplace=True)
        return combinedDataFrame
        
    def plotDifferenceBetweenDeathTolls(self):
        listOfFrames= []
        listOfFrames.append(self.getDataFrameWithTollDifference("2020-11-11", "2020-11-09", self.countryName))
        neighbourNames = self.getNeighbourNames()
        
        listOfFrames.append(self.getDataFrameWithTollDifference("2020-11-11", "2020-11-09", neighbourNames[0][0]))
        if len(neighbourNames) > 1:
            listOfFrames.append(self.getDataFrameWithTollDifference("2020-11-11", "2020-11-09", neighbourNames[1][0]))
        
        dataFrame = panda.concat(listOfFrames)
        print(dataFrame)
        dataFrame['deaths1Mpop'].plot(kind='bar', title="Difference Between Death Tolls of Country and Neighbours")
        
    def getNeighbourNames(self):
        neighbours = self.dbManager.select_data_from_table('neighbours', 'neighbour', f"country='{self.countryName}'")
        return neighbours
        
    def trendLineplot(self):
        listOfFrames=[]
        listOfFrames.append(self.getDataFrameForDayAndCountry('2020-11-11', self.countryName, 'newCases'))
        neighbourNames= self.getNeighbourNames()
        
        for name in neighbourNames:
            #print("NAME OF NEIGHBOUR" + name[0])
            listOfFrames.append(self.getDataFrameForDayAndCountry('2020-11-11', name[0], 'newCases'))
        
        dataFrame = panda.concat(listOfFrames)
        #sns.set_theme(color_codes=True)
        #x,y= pand.Series(x, name="x_var"), pand.Series(y, name="y_var")
        #tips = sns.load_dataset("tips")
        print(dataFrame)
        sns.lmplot(x = "Countries", y = "newCases", data= dataFrame);
        plt.show()
        
        
    
    def getDataFrameForDayAndCountry(self, day, currentCountryName, columns):
        if currentCountryName == "United States":
            currentCountryName = "USA"
        
        countryQuery = f"SELECT {columns} FROM countryData WHERE country ='{currentCountryName}' AND dataDate='{day}'"
        frameData = panda.read_sql_query(countryQuery, self.dbManager.connectionObject)
        newIndex= {0: currentCountryName}
        frameData.rename(index=newIndex,inplace=True)
        #print(frameData)
        return panda.DataFrame(frameData)

dbManager = DatabaseManager('localhost', 'root', 'dawson', 'corona_stats')
dbManager.createTables()

dbManager.fillCountryTable()
dbManager.fillNeighboursTable()

plot = PlotManager(dbManager, "Canada")
diff = plot.trendLineplot()
dbManager.closeConnection()
