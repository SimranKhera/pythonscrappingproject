# -*- coding: utf-8 -*-
"""

Simranjot Kaur Khera
1837165
"""
import mysql.connector
import datetime
from scraping import Scraper
from my_file_io import FileIO
import pandas as pd

class DatabaseManager:

    
    #gets a connection object and creates a cursor.
    def __init__(self, userHost, userName, userPassword, databaseName):

        self.connectionObject= self.createConnectionObject(databaseName, userHost, userName, userPassword)
        self.cursor = self.connectionObject.cursor()
        

    #Creates connection to database. If the given database does not exist, it creates 
    # a connection without database access -> the database must the be created with create_db()
    #It handles an exception in case of connection problems.
    def createConnectionObject(self, databaseName, userHost, userName, userPassword):
        try:
            if databaseName != None:
                connectionObject = mysql.connector.connect(
                    host=userHost,
                    user= userName,
                    password=userPassword,
                    database= databaseName)
                return connectionObject
            else:
                connectionObject = mysql.connector.connect(
                    host=userHost,
                    user= userName,
                    password=userPassword)
                return connectionObject
            
        except mysql.connector.Error as error:
            print(f"Issue with connection to database: {error}")

    #Given a database name, creates the database with current connection
    #If database already exists, it will print a message saying there's an error and 
    #not try to create a database.
    def create_db(self, databaseName):
        if not self.findDB(databaseName):
            self.cursor.execute(f"CREATE DATABASE {databaseName}")
        else:
            print("Error- Database already exists. Try again")
    
    #Used to select a database to query from
    def select_db(self,dbName):
        self.cursor.execute(f"USE {dbName}")

    # Given a name, schema, this function creates an empty table with
    # the given schema. 
    def create_table(self, tableName, tableSchema):
        self.cursor.execute("DROP TABLE IF EXISTS "+tableName)
        self.cursor.execute(f"CREATE TABLE {tableName} {tableSchema}")
    
    # Selects given columns from the table matching given criteria
    def select_data_from_table(self, tableName,columns, criteria):
        if criteria == None:
            self.cursor.execute(f"SELECT {columns} FROM {tableName}")
        else:
            print(f"SELECT {columns} FROM {tableName} WHERE {criteria}")
            self.cursor.execute(f"SELECT {columns} FROM {tableName} WHERE {criteria}")
        return self.cursor.fetchall()
    
    # insert data into table with given name and schema.
    # Contents of tuplelist is put into the table
    def populate_table(self, tableName, tupleList):
        insertQuery=f"INSERT INTO {tableName} VALUES {tupleList}"
        #print(f"TUPLE ABOUT TO BE ADDED {tupleList}")
        self.cursor.execute(insertQuery)

    # This method checks if the db already exists in mysql,
    # returns true if it does. 
    # This helper method is used while creating the database
    def findDB(self,dbName):
        self.cursor.execute("SHOW DATABASES")
        currentDatabases=self.cursor.fetchall()
    
        for element in currentDatabases:
            if dbName.lower()==element[0].lower():
                return True
        return False

    # Closes both the connection object and cursor.
    def closeConnection(self):
        if self.connectionObject.is_connected():
            self.cursor.close()
            self.connectionObject.close()
    
    # Creates the necessary tables for this project.
    # 2 tables are created: countryData and neighbours
    def createTables(self):
        self.create_table('countryData', """(
      id int(11) auto_increment , 
      dataDate varchar(20) NOT NULL,
      country varchar(50) NOT NULL,
      ranking int(11) NOT NULL,
      totalCases int(11),
      newCases int(11),
      totalDeaths int(11),
      newDeaths int(11),
      totalRecovored int(11),
      newRecovered int(11),
      activeCases int(11),
      critical int(11),
      totCases1Mpop int(11),
      deaths1Mpop decimal (10,2),
      totalTests int(11),
      tests1Mpop int(11),
      population int(11),
      continent varchar(50),
      caseEveryXppl int(10),
      deatheveryXppl int(10),
      testeveryXppl int(10), 
      PRIMARY KEY (id)
      );""")
        
        self.create_table('neighbours', """(
        country varchar(50) NOT NULL,
        neighbour varchar(50) DEFAULT 0,
        distance int(11) DEFAULT 0,
        PRIMARY KEY (country, neighbour)
        );""")
        
    def fillCountryTable(self):
        #fill country data
        fileIO= FileIO("jsonFile.txt")
        countryResult= fileIO.readFromJsonFile()
        fileIO.closeFile()
        self.fillCountryDataTable(countryResult)
        
    def fillNeighboursTable(self):
        #fill neighbour data
        fileIO= FileIO("contry_neighbour_dist_file.json")
        result = fileIO.readFromJsonFile()
        fileIO.closeFile()
        self.fillNeighboursDataTable(result)

    # This method is used to add Data to the Country Table, it uses the data from a list from a json file
    def fillCountryDataTable(self, resultList):
        #countryData=[]
        countryObjCountr=1
        for countryObj in resultList:
            date= list(countryObj.keys())[0]
            country = list(countryObj.get(date).keys())[0]
            
            tempDataList=[]
            tempDataList.append(countryObjCountr)
            tempDataList.append(date)
            tempDataList.append(country)
            
            index = 0
            #looping through the rest of the values.
            for countryStat in countryObj.get(date).get(country):
                currentRowValue= countryObj.get(date).get(country).get(countryStat)
                #data at 14th position is a string, the rest are integers
                if  index ==14:
                    tempDataList.append(currentRowValue)
                elif '.' in currentRowValue:
                    tempDataList.append(float(currentRowValue))
                else:
                    #if there is no number provided for a column, we want -1 to be stored in the table
                    if (currentRowValue == "" or currentRowValue == " " or currentRowValue == 'N/A'):
                        tempDataList.append(-1)
                    else:
                        tempDataList.append(int(currentRowValue))
                    
                index+=1
                
            self.populate_table('countryData', tuple(tempDataList))
            countryObjCountr+=1
        #return countryData

    # It populates the Neighbours table with a list whose content comes from a json file
    def fillNeighboursDataTable(self, resultList):
        duplicate= False
        
        for countryObj in resultList:
            #getting the first key which is the country name
            currentCountryName = list(countryObj.keys())[0]
            #using country name to get all neighbours of the country
            neighbours = countryObj.get(currentCountryName)
            
            #loop through neighbours, get distance, create tuple and add to database
            for neighbourCountryName in neighbours:
                distance = neighbours.get(neighbourCountryName)
                if distance == "" or distance==" ":
                    distance= -1
                
                if currentCountryName == 'Israel' and neighbourCountryName == 'state of palestine':
                    continue
                else:
                    row = (currentCountryName, neighbourCountryName,distance)

                self.populate_table('neighbours', row)
"""
#TESTING PURPOSES
#create connection, create database
dbManager = DatabaseManager('localhost', 'root', 'dawson', 'corona_stats')
#dbManager.create_db('corona_stats')

    #use new database created
dbManager.select_db('corona_stats')

#create table
dbManager.createTables()

#fill country data
fileIO= FileIO("jsonFile.txt")
countryResult= fileIO.readFromJsonFile()
fileIO.closeFile()
dbManager.fillCountryDataTable(countryResult)

allDataFromCountries = dbManager.select_data_from_table('countrydata', '*', None)

#fill neighbour data
fileIO= FileIO("contry_neighbour_dist_file.json")
result = fileIO.readFromJsonFile()
fileIO.closeFile()
dbManager.fillNeighboursDataTable(result)

allDataFromNeighbour = dbManager.select_data_from_table('neighbours', '*',None)



dbManager.closeConnection()


scrapDate = datetime.date(2020,11,11)
scraper = Scraper("https://web.archive.org/web/20201111170613/https://www.worldometers.info/coronavirus/", "WednesdayData.html",scrapDate)
test = scraper.getAllData()
#t= scraper.getYesterday2Data()
"""
