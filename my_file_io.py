# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 10:57:22 2020

@author: Simranjot Kaur Khera
"""
import json
class FileIO:


    def __init__(self,filename):
        self.fileName=filename
        self.fileobj = open(self.fileName, 'r')
        
    def readFromFile(self):
        htmlCode = self.fileobj.read()
        return htmlCode

    def writeToFile(self,htmlText):
        self.fileobj=open(self.fileName,'wb');
        self.fileobj.write(htmlText.read())

    def writeJSONToFile(self, countryDataObjects):
        self.fileobj = open(self.fileName, 'w')
        jsonString= ""
        for element in countryDataObjects:
            jsonString+= json.dumps(element, indent=4)
        self.fileobj.write(jsonString)
    
    #returns a list of dictionaries with json data stored in file
    def readFromJsonFile(self):
        jsonText= self.fileobj.read()
        return json.loads(jsonText)
    
    def closeFile(self):
        self.fileobj.close()


