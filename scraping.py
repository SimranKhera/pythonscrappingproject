# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 15:21:06 2020

@author: Simranjot Kaur Khera
"""
from my_file_io import FileIO
from urllib.request import urlopen, Request
from bs4 import BeautifulSoup
import json
import datetime

class Scraper:
    
    #Needs url of the site to scrape, the filename where the scraped html
    #must be saved and the date of the day when the website was scrapped or is about to be scrapped.
    #The filename must have the extension in it.
    def __init__(self, siteURL, fileName, date):
        self.request= Request(siteURL, headers={'user-agent':'Mozilla/5.0'})
        self.webisteHtml= ""
        self.fileIO = FileIO(fileName)
        self.date = date
        self.bsObject = BeautifulSoup(self.fileIO.readFromFile(), 'html.parser')
    
    #Scrapes the given url in init, stores html in websiteHtml and writes itthe html to file
    def scrapeWebsite(self):
        self.webisteHtml =urlopen(self.request)
        self.fileIO.writeToFile(self.webisteHtml)
    
    #Returns all of the data for Wednesday, the day before and 2 days before. 
    #This method then writes the acquired data into Json file.
    #Uses helper methods for getting the data for each day. 
    def getAllData(self):
        #appending all days data into 1 list
        countryDataObjects = []
        countryDataObjects.append(self.getTodaysData())
        countryDataObjects[0] += self.getYesterdayData()
        countryDataObjects[0] +=self.getYesterday2Data()
        
        #storing data in json file
        tempFileManager = FileIO("jsonFile.txt")
        tempFileManager.writeJSONToFile(countryDataObjects)
        tempFileManager.closeFile()
        return countryDataObjects
    
    #Uses BeautifulSoup to read the html stored in FileIO object.
    #It then finds the table for Wednesdays data and uses getDataFromRows() function
    #to get CountryData objects
    def getTodaysData(self):
        htmlTableRows = self.bsObject.find(id='nav-today').tbody.find_all('tr')
        #getting country data for Wednesday the 11th
        return self.getDataFromRows(htmlTableRows, self.date)
    
    #Uses BeautifulSoup to read the html stored in FileIO object.
    #It then finds the table for the day before Wednesday data and uses getDataFromRows() function
    #to get CountryData objects
    def getYesterdayData(self):
        htmlTableRows = self.bsObject.find(id='nav-yesterday').tbody.find_all('tr')
        daysToRemove = datetime.timedelta(1)
        yesterdayDate = self.date - daysToRemove
        return self.getDataFromRows(htmlTableRows, yesterdayDate)

    #Uses BeautifulSoup to read the html stored in FileIO object.
    #It then finds the table for 2 days before Wednesday data and uses getDataFromRows() function
    #to get CountryData objects
    def getYesterday2Data(self):
        htmlTableRows = self.bsObject.find(id='nav-yesterday2').tbody.find_all('tr')
        daysToRemove = datetime.timedelta(2)
        previousTwoDay = self.date - daysToRemove
        return self.getDataFromRows(htmlTableRows, previousTwoDay)
    
    # Goes through given table row tags and retrieves the text of each table data. 
    # Using the text, it create an array of countrydata objects (each countrydata object represents 
    # one row in the table). 
    def getDataFromRows(self, htmlTableRows, date):
        countryDataObjects = []
        tableDataTagText=[]
        
        #looping through the rows in the tables
        for rowIndex in range(len(htmlTableRows)):
            # check if the row is a country by checking if there is a rank
            #the rank is always in the first tdTag
            if htmlTableRows[rowIndex].find('td').text != "":
                tableDataTags = htmlTableRows[rowIndex].find_all('td')
                
                #looping through each td tags in the table row
                for tableDataTag in tableDataTags:
                    #removing plus sign in front of new cases and other fields
                    if not not tableDataTag.text and tableDataTag.text[0] == '+':
                        text = tableDataTag.text[1:].replace(",","")
                    else:
                        text = tableDataTag.text.replace(",","")
                    
                    tableDataTagText.append(text)
                print(f"Index at {rowIndex} with country ID : {tableDataTagText[0]}")
                #creating countrydata object to add to countryDataObjects array
                countryDataObj= self.createCountryDataObject(tableDataTagText, date)
                
                countryDataObjects.append(countryDataObj)
                #emptying td texts for the next rows
                tableDataTagText= []
        return countryDataObjects
    
    #Given an array of text from all table data tags in a row, this function creates 
    # a single countryData object. The date is for the perticular day the countryData is created for. 
    # This is a helper function used to create data objects from given data.
    def createCountryDataObject(self, tableDataTagText, date):
        countryDataObj = {date.strftime('%Y-%m-%d') : 
                          {tableDataTagText[1]: 
                          {'rank': tableDataTagText[0],'totalCases': tableDataTagText[2],
                          'newCases':tableDataTagText[3], 'totalDeaths':tableDataTagText[4],
                          'newDeaths':tableDataTagText[5], 'totalRecovored':tableDataTagText[6],'newRecovered': tableDataTagText[7],
                          'activeCases' : tableDataTagText[8], 'critical': tableDataTagText[9], 'totCases1Mpop':tableDataTagText[10],
                          'deaths1Mpop': tableDataTagText[11], 'totalTests':tableDataTagText[12], 'tests1Mpop': tableDataTagText[13],
                          'population':tableDataTagText[14], 'continent': tableDataTagText[15], 'caseEveryXppl':tableDataTagText[16],
                          'deatheveryXppl':tableDataTagText[17], 'testeveryXppl': tableDataTagText[18]
                          }
                          }
                          }
        return countryDataObj
"""
scrapDate = datetime.date(2020,11,11)
scraper = Scraper("https://web.archive.org/web/20201111170613/https://www.worldometers.info/coronavirus/", "WednesdayData.html",scrapDate)
test = scraper.getAllData()
"""


