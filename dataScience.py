import pandas as myPanda
import matplotlib
import numpy
from databaseManager import DatabaseManager
from my_file_io import FileIO


class dataScience:
    # init method, receives the name of a country and a database
    def __init__(self, theCountry, db):
        self.country = theCountry
<<<<<<< HEAD
        self.db= db
        self.db.fillCountryTable()
        self.db.fillNeighboursTable()
=======
        self.db = db
>>>>>>> 0932751b7dedf7f1448abfa33632d794b4a7e071
        self.all_data = self.createDataFrame()
        self.neighbour_country_data = self.getNeighbourCountryDataFrame()
        self.plotCountryData(self.all_data)
        self.plotCountryData(self.neighbour_country_data)

    # Creates and returns a DataFrame object based on the country parameter from the init method
    def createDataFrame(self):
        frameData = myPanda.read_sql_query(
            f"SELECT totalCases, totalTests, totalRecovored, totalDeaths, population from countryData where country = '{self.country}'",
            self.db.connectionObject)
        return myPanda.DataFrame(frameData)

    # This method gets the neighbouring country's data from the database and returns it as a DataFrame object
    def getNeighbourCountryDataFrame(self):
        frameData = myPanda.read_sql_query(
            f"SELECT neighbour, max(distance) from neighbours where country = '{self.country}'",
            self.db.connectionObject)
        neighbour_data = myPanda.DataFrame(frameData)
        neighbourCountry = neighbour_data['neighbour'].to_string().split("0    ")[1]
        if neighbourCountry == 'United States':
            neighbourCountry = "USA"
        neighbourCountryData = myPanda.read_sql_query(
            f"SELECT totalCases, totalTests, totalRecovored, totalDeaths, population from countryData where country='{neighbourCountry}'",
            self.db.connectionObject)

        return myPanda.DataFrame(neighbourCountryData)
    # Helper method that uses a dataFrame object and makes into a dictionary with clean data
    def __countryDictionary(self, df):
        countryDictionary = self.__getDictionaryOfColumns(self.__getColumnDataOfCountry(df))
        return countryDictionary

    # Returns a clean dataFrame, it removes the -1 values. It uses countryDictionary to clean the data
    # It transforms the dictionary into a DataFrame object
    def __returnCleanData(self, df, days=3):
        if days != 3:
            df = df.head(days)
        dataFrame = myPanda.DataFrame(self.__countryDictionary(df))
        return dataFrame

    # Plots each of the key fields of a country
    def plotCountryData(self, df):
        dataFrame = self.__returnCleanData(df)
        dataframe = dataFrame.plot(kind='hist', title='Country Covid19 Evolution')
<<<<<<< HEAD
        

=======

    # Prints all the data from the specified country dataFrame
>>>>>>> 0932751b7dedf7f1448abfa33632d794b4a7e071
    def showData(self):
        print(self.all_data.info())
    

<<<<<<< HEAD

=======
>>>>>>> 0932751b7dedf7f1448abfa33632d794b4a7e071
    # Returns array with data of each column without null values
    def __getColumnDataOfCountry(self, df):
        totalCases = df['totalCases'].tolist()
        totalCases = self.__removeItem(totalCases)

        totalTests = df['totalTests'].tolist()
        totalTests = self.__removeItem(totalTests)

        totalRecovored = df['totalRecovored']
        totalRecovored = self.__removeItem(totalRecovored)

        totalDeaths = df['totalDeaths']
        totalDeaths = self.__removeItem(totalDeaths)
        population = df['population']
        population = self.__removeItem(population)

        return [totalCases, totalTests, totalRecovored, totalDeaths, population]

    # It takes a DataFrame with the data of key fields from a specific country
    def __getDictionaryOfColumns(self, countryData):
        myDictionary = {}
        print(countryData[0])
        myDictionary['totalCases'] = countryData[0]
        print(countryData[1])
        myDictionary['totalTests'] = countryData[1]
        myDictionary['totalRecovored']: countryData[2]
        myDictionary['totalDeaths'] = countryData[3]
        myDictionary['population'] = countryData[4]
        return myDictionary

    # This helper method removes all entries on the list that contain a -1
    def __removeItem(self, list):
        for y in range(0, len(list)):
            if list[y] == -1:
                list.pop(y)
        return list

    # This helper method get the correlation between specifics columns, it is not used
    def __getCorrelationBetweenFields(self, countryData):
        myDictionary = {}
        # totalDeath and TotalTests
        myDictionary['totalDeath_TotalTests'] = countryData[3].corr(countryData[1], method='pearson')
        # Population and TotalCases
        myDictionary['population_TotalCases'] = countryData[4].corr(countryData[0], method='pearson')
        # TotalTests and totalCases
        myDictionary['totalTests_TotalCases'] = countryData[1].corr(countryData[0], method='pearson')
        # TotalDeaths and TotalRecovered
        myDictionary['totalDeaths_TotalRecovered'] = countryData[3].corr(countryData[2], method='pearson')
        # transform dic into dataframe, data.plot.
        return myDictionary

'''
#Must do this once on your machine
dbManager = DatabaseManager('localhost', 'root', 'dawson', None)
dbManager.create_db('corona_stats')
dbManager.select_db('corona_stats')

'''
"""
# once you have run the above code, you can just do this to get a dbobject
dbManager = DatabaseManager('localhost', 'root', 'dawson', 'corona_stats')
ds = dataScience('Canada', dbManager)
print(ds.all_data.head())
dbManager.closeConnection()
"""
